# Ketcher
## 介绍
http://lifescience.opensource.epam.com/download/ketcher.html

## 构建
### 环境
```
➜  ~ nvm --version
0.33.8
➜  ~ node -v
v8.5.0
➜  ~ npm -v
5.6.0
```

### 安装grunt
```
➜  ~ npm update -g npm
➜  ~ npm install -g grunt-cli
```

### 构建项目
* 修改Gruntfile
```
修改raphael-min.js为raphael.min.js
```
* 修改package.json
```
删除"assemble": "~0.4.41",
```
* 编译
```
➜  ketcher git:(master) ✗ npm install
➜  ketcher git:(master) ✗ npm install grunt-assemble --save-dev
➜  ketcher git:(master) ✗ grunt
```





